# report_to_misp
Parse a report and import the attributes into a MISP


```
usage: report_to_misp.py [-h] [-r REPORT] [-nv] [-s SERVER] [-a AUTH] [-p PAGE] [--ssl] [-o ORG] [-d {org,community,connected,all}] [-t {undefined,low,medium,high}] [-S] [-sp SEARCH]

optional arguments:
  -h, --help            show this help message and exit
  -r REPORT, --report REPORT
                        Path to the file or directory of report(s) to parse
  -nv, --no_verify      Don't request the user to verify the type and use of each indicator
  -s SERVER, --server SERVER
                        The MISP server address
  -a AUTH, --auth AUTH  Your authentication key to access the MISP server API
  -p PAGE, --page PAGE  The page to begin looking for indicators on (used for reports with an indicator appendix)
  --ssl                 Use SSL for accessing the MISP server
  -o ORG, --org ORG     The organization that created the event
  -d {org,community,connected,all}, --distribution {org,community,connected,all}
                        Who should be able to view the event
  -t {undefined,low,medium,high}, --threat_level {undefined,low,medium,high}
                        The threat level of the event
  -S, --sigma           Export Sigma rules
  -sp SEARCH, --search SEARCH
                        Search pattern for exported suricata rules. See https://www.circl.lu/doc/misp/automation/index.html#search
```

https://github.com/PaloAltoNetworks/report_to_misp


## Requirements
pip install -r ioc_parser/requirements.txt

# ioc-parser
IOC Parser is a tool to extract indicators of compromise from security reports in PDF format. A good collection of APT related reports with many IOCs can be found here: [APTNotes](https://github.com/kbandla/APTnotes).

## Usage
**iocp [-h] [-p INI] [-i FORMAT] [-o FORMAT] [-d] [-l LIB] FILE**
* *FILE* File/directory path to report(s)/Gmail account in double quotes ("username@gmail.com password")
* *-p INI* Pattern file
* *-i FORMAT* Input format (pdf/txt/docx/html/csv/xls/xlsx/gmail)
* *-o FORMAT* Output format (csv/json/yara/netflow)
* *-d* Deduplicate matches
* *-l LIB* Parsing library


## Dependencies

* [docx2txt](http://docx2txt.sourceforge.net/)

## Requirements
One of the following PDF parsing libraries:
* [PyPDF2](https://github.com/mstamy2/PyPDF2) - *pip install pypdf2*
* [pdfminer](https://github.com/euske/pdfminer) - *pip install pdfminer*

For HTML parsing support:
* [BeautifulSoup](http://www.crummy.com/software/BeautifulSoup/) - *pip install beautifulsoup4*

For HTTP(S) support:
* [requests](http://docs.python-requests.org/en/latest/) - *pip install requests*

For XLS/XLSX support:
* [xlrd](https://github.com/python-excel/xlrd) - *pip install xlrd*

For Gmail support:
* [gmail](https://github.com/charlierguo/gmail)

For Sigma support
```
apt install golang-go
go get github.com/0xThiebaut/sigmai
```

## Merged changes from forks:

[@buffer](https://github.com/buffer/ioc_parser/)

[@dadokkio](https://github.com/dadokkio/ioc_parser/)

[@LDO-CERT](https://github.com/LDO-CERT/ioc_parser/)
